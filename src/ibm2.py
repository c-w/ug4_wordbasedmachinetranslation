from __future__ import division
import argparse
import ibm1
import logging
import sys
import time


###############################################################################
###############################################################################
################################# IBM 2 stuff #################################
###############################################################################
###############################################################################


def train(source, target, t, miniter=2, maxiter=-1, roundoff=-1):
    now = time.time
    # get parallel corpora
    PC = ibm1.read_parallel_sentences(source, target)
    PC, encoder = ibm1.encode(PC)
    E, F = ibm1.extract_vocabulary(PC)
    LE_LF = set((len(e_s), len(f_s)) for e_s, f_s in PC)
    # initialize the distribution t(e|f) with the values from Model 1 training
    t = ibm1.read_translation_table(t, encoder=encoder)
    a = dict(((j, le, lf), dict((i, 1 / (lf + 1)) for i in irange(0, lf)))
             for le, lf in LE_LF for j in irange(1, le))
    # run the EM algorithm
    n = 0
    while True:
        _t = now()
        if n > miniter:
            if maxiter > 0 and n >= maxiter:
                break
            if converged(t, a, PC, roundoff=roundoff):
                break
        n += 1

        # initialize
        count = dict((f, dict((e, 0) for e in E)) for f in F)
        total = dict((f, 0) for f in F)
        count_a = dict(((j, le, lf), dict((i, 0) for i in irange(0, lf)))
                       for le, lf in LE_LF for j in irange(1, le))
        total_a = dict(((j, le, lf), 0)
                       for le, lf in LE_LF for j in irange(1, le))
        for e_s, f_s in PC:
            le = len(e_s)
            lf = len(f_s)
            # compute normalization
            s_total = dict()
            for j, ej in enumerate(e_s, start=1):
                s_total[ej] = 0
                for i, fi in enumerate(f_s, start=0):
                    s_total[ej] += t[fi][ej] * a[j, le, lf][i]
            # collect counts
            for j, ej in enumerate(e_s, start=1):
                j_le_lf = j, le, lf
                for i, fi in enumerate(f_s, start=0):
                    c = t[fi][ej] * a[j_le_lf][i] / s_total[ej]
                    count[fi][ej] += c
                    total[fi] += c
                    count_a[j_le_lf][i] += c
                    total_a[j_le_lf] += c

        # estimate probabilities
        t = dict((f, dict((e, 0) for e in E)) for f in F)
        a = dict(((j, le, lf), dict((i, 0) for i in irange(0, lf)))
                 for le, lf in LE_LF for j in irange(1, le))
        for e in E:
            for f in F:
                t[f][e] = count[f][e] / total[f]
        for le, lf in LE_LF:
            for j in irange(1, le):
                j_le_lf = j, le, lf
                for i in irange(0, lf):
                    a[j_le_lf][i] = count_a[j_le_lf][i] / total_a[j_le_lf]
        logging.debug('finished EM iteration %d in %.2fs' % (n, now() - _t))

    logging.info('converged after {niter} iterations with perplexity {pp:.6f}'
                 .format(niter=n, pp=perplexity(t, a, PC)))
    ibm1.consistency_check(t)
    ibm1.consistency_check(a)
    t = ibm1.decode(t, encoder)
    return t, a


def converged(t, a, PC, roundoff=-1):
    """Checks if the values in the translation table have converged by looking
    at changes in the perplexity of the training corpus
    """

    pp_last = converged._pp_last
    pp_now = perplexity(t, a, PC)

    if roundoff > 0:
        fmt = '%.{roundoff}f'.format(roundoff=roundoff)
        pp_last = float(fmt % pp_last)
        pp_now = float(fmt % pp_now)

    converged._pp_last = pp_now
    return ibm1.flt_eq(pp_last, pp_now)
converged._pp_last = -float('inf')


def perplexity(t, a, PC):
    return -sum(ibm1.log2(translation_probability(t, a, e, f)) for e, f in PC)


def translation_probability(t, a, e_s, f_s):
    lf = len(f_s)
    le = len(e_s)
    return 1 / (lf ** le) * sum(sum(t[fi][ej] * a[j, le, lf][i]
                                    for i, fi in enumerate(f_s))
                                for j, ej in enumerate(e_s, start=1))


def write_distortion_table(distortion_table, outfile):
    for (j, le, lf), vals in distortion_table.iteritems():
        for i, prob in vals.iteritems():
            outfile.write('{sl} {tl} {sp} {tp} {prob}\n'
                          .format(sl=lf, tl=le, sp=i, tp=j, prob=prob))


def read_distortion_table(infile, delim=' '):
    parse_line = lambda l: map(int, l[0:4]) + [float(l[4])]
    a = dict()
    for line in infile:
        lf, le, i, j, prob = parse_line(line.strip().split(delim))
        j_le_lf = j, le, lf
        if not j_le_lf in a:
            a[j_le_lf] = dict()
        a[j_le_lf][i] = prob
    return a


def align(source, target, t, a, translate=False):
    PC = ibm1.read_parallel_sentences(source, target)
    t = ibm1.read_translation_table(t)
    a = read_distortion_table(a)
    alignments = []
    for e_s, f_s in PC:
        le = len(e_s)
        lf = len(f_s)
        alignment = []
        for j, e in enumerate(e_s, start=1):
            best_p = None
            best_a = None
            for i, f in enumerate(f_s):
                p = t[f][e] * a[j, le, lf][i]
                if p > best_p:
                    best_p = p
                    best_a = i
            alignment.append(best_a if not translate else f_s[best_a])
        alignments.append(alignment)
    return alignments


###############################################################################
###############################################################################
################################ Utility stuff ################################
###############################################################################
###############################################################################


def irange(start, stop, step=1):
    """Returns an iterator over the range [start, stop].
    Unlike xrange, irange includes the end-point in the returned iterator.
    """

    return xrange(start, stop + 1, step)


###############################################################################
###############################################################################
##################################### Main ####################################
###############################################################################
###############################################################################


def do_apply(args=None):
    # setup command line interface
    parser = ibm1.DefaultArgumentParser(description='Applies IBM model 2')
    parser.add_argument('source', type=argparse.FileType('r'),
                        help='path to corpus in source language')
    parser.add_argument('target', type=argparse.FileType('r'),
                        help='path to corpus in target language')
    parser.add_argument('trans', action='store', type=argparse.FileType('r'),
                        help='translation table from IBM model 2')
    parser.add_argument('dist', action='store', type=argparse.FileType('r'),
                        help='distortion table from IBM model 2')
    parser.add_argument('output', type=argparse.FileType('w'),
                        help='output file for alignments')
    parser.add_argument('--translate', action='store_true', default=False,
                        help='output translations instead of alignments')
    # parse arguments
    args = parser.parse_args(args)
    source = args.source
    target = args.target
    translation = args.trans
    distortion = args.dist
    outfile = args.output
    translate = args.translate

    # apply IBM model 2 and output results
    try:
        alignments = align(source, target, translation, distortion, translate)
        ibm1.write_alignments(alignments, outfile)
    finally:
        source.close()
        target.close()
        translation.close()
        distortion.close()
        outfile.close()


def do_train(args=None):
    # setup command line interface
    parser = ibm1.ModelTrainingArgumentParser(description='Trains IBM model 2')
    parser.add_argument('source', action='store', type=argparse.FileType('r'),
                        help='path to corpus in source language')
    parser.add_argument('target', action='store', type=argparse.FileType('r'),
                        help='path to corpus in target language')
    parser.add_argument('ibm1', action='store', type=argparse.FileType('r'),
                        help='translation table from IBM model 1')
    parser.add_argument('out1', action='store', type=argparse.FileType('w'),
                        help='path to output translation table')
    parser.add_argument('out2', action='store', type=argparse.FileType('w'),
                        help='path to output distortion table')
    # parse arguments
    args = parser.parse_args(args)
    source = args.source
    target = args.target
    ibm1_file = args.ibm1
    translation_file = args.out1
    distortion_file = args.out2
    roundoff = args.roundoff
    miniter = args.miniter
    maxiter = args.maxiter

    # run IBM model 2 and output results
    try:
        translation, distortion = train(
            source, target, ibm1_file,
            roundoff=roundoff, miniter=miniter, maxiter=maxiter)
        ibm1.write_translation_table(translation, translation_file)
        write_distortion_table(distortion, distortion_file)
    finally:
        source.close()
        target.close()
        ibm1_file.close()
        translation_file.close()
        distortion_file.close()


if __name__ == '__main__':
    ibm1.main(modelno=2, module=sys.modules[__name__])
