#!/bin/bash

set -o nounset
set -o errexit

readonly scriptdir="$(dirname $(readlink -f $0))"

function add_to_gitignore() {
    local gitignore="$scriptdir/.gitignore"
    local files="$@"
    [[ ! -f "$gitignore" ]] && > "$gitignore"
    for f in $@; do
        echo $f
        f=$(basename $f)
        grep -q "$f" "$gitignore" || echo "$f" >> "$gitignore"
    done
}

function get_aln2latex() {
    local data_url='http://www.statmt.org/mt-course/assignment1/aln2latex.py'
    local outpath="$scriptdir/$(basename $data_url .py)"
    [[ -d $outpath ]] && return
    echo "Fetching MultEval"
    mkdir -p $outpath
    wget $data_url -O $outpath/aln2latex.py
    sed -i 's,\\usepackage\[utf8\],\\usepackage[utf8x],' $outpath/*.py
    chmod +x $outpath/*.py
    add_to_gitignore $outpath
    echo "done"
}

function get_multeval() {
    local data_url='https://github.com/jhclark/multeval.git'
    local outpath="$scriptdir/$(basename $data_url .git)"
    [[ -d $outpath ]] && return
    echo "Fetching MultEval"
    git clone $data_url
    pushd . > /dev/null && cd $outpath
    rm -rf .git
    ./multeval.sh eval --refs example/refs.test2010.lc.tok.en.* \
                       --hyps-baseline example/hyps.lc.tok.en.baseline.opt* \
                       --meteor.language en && errco=$? || errco=$?
    ant
    popd > /dev/null
    add_to_gitignore $outpath
    echo "done"
}

function main() {
    [[ -z $@ ]] && echo "please specify package to fetch" && exit 1
    for arg in "$@"; do
        case $arg in
            aln2latex) get_aln2latex ;;
            multeval) get_multeval ;;
            *) echo "unknown argument '$arg'"
        esac
    done
}

main "$@"
