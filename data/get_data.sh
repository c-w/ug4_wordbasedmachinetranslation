#!/bin/bash

set -o nounset
set -o errexit

readonly scriptdir="$(dirname $(readlink -f $0))"

function add_to_gitignore() {
    local gitignore="$scriptdir/.gitignore"
    local files="$@"
    [[ ! -f "$gitignore" ]] && > "$gitignore"
    for f in $@; do
        grep -q "$f" "$gitignore" || echo "$f" >> "$gitignore"
    done
}

function fetch_tgz() {
    local url="$1"
    local tgz="$(basename $url)"
    wget "$url"
    tar vxf "$tgz"
    add_to_gitignore "$(tar tf $tgz)"
    rm -f "$tgz"
}

function main() {
    [[ -z $@ ]] && echo "please specify package to fetch" && exit 1
    local dataroot='http://www.statmt.org/mt-course/assignment1'
    local lang=""
    local size=""
    for arg in "$@"; do
        case $arg in
            toy) fetch_tgz "$dataroot/toy.tgz" && exit ;;
            fr|de|es|cs|ro|it) lang="$arg" ;;
            tiny|small) size="$arg" ;;
            *) echo "unknown argument '$arg'"
        esac
    done
    ([[ -z $lang ]] || [[ -z $size ]]) && \
        echo "need to specify parallel corpus language and size" && exit
    fetch_tgz "$dataroot/$lang-en.$size.tgz"
}

main "$@"
